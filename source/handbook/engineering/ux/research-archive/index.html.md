---
layout: markdown_page
title: "UX Research archive"
---

### On this page

{:.no_toc}

- TOC
{:toc}

## Research archive

The following is a collection of UX Research broken down by specific areas of GitLab. It is intended to shed insight into key UX design decisions and the data that informed those decisions.

Navigating the UX Research Archive: To go directly to the research report, click the link listed under the research area (e.g. "DevOps Insights: Operations Survey Report"). To view summaries of all studies related to a certain feature or concept, click the link to the summary of the research area (e.g. "Summary of 'Dashboards' Research").

### CI/CD
2018-05 [CI/CD Adoption Research - Survey Report](https://drive.google.com/file/d/1i8h93tVCv8v5TFMvOyfTdeP04mT3tx7l/view?usp=sharing)

2018-05 [CI/CD Adoption Research - Interview Report](https://drive.google.com/file/d/1y2KwX5j8ODGd3Ajn-ybYWeFJzEPL4zL9/view?usp=sharing)

### Dashboards

2017-08 [Dashboards (Workflow User Interviews)](https://drive.google.com/file/d/19kpqVONthqacj93DniXEUZbwWO0J_XYt/view?usp=sharing)

### DevOps
2018-05 [Discoverability of Auto DevOps](https://drive.google.com/file/d/1jiDfUSibPh94cDuN4YvvhsCAMR8XGVAf/)

2018-03 [DevOps Insights: Operations Survey Report](https://drive.google.com/file/d/1sgPETOKc53QBLvd438gPljRRjeBOm8jr/) 

2018-03 [DevOps Insights: Operations User Interviews Report](https://drive.google.com/file/d/18r1cU8NVxFEHcI6RaJEQ_c-_-7SCNpAr/)

### Issues

2018-01 [Issue display & sort order](https://gitlab.com/gitlab-org/ux-research/issues/39)

2017-11 [Distinguishing between an issue and merge request](https://drive.google.com/file/d/1iuvAIaIp0yrTaBrU2yOWgZCoMhpQaq1o/view?usp=sharing)

2017-11 [Moving an issue between projects](https://drive.google.com/file/d/1BxH7MniOWtou3vCxt57voISTmOAwWKfN/view?usp=sharing)

2017-08 [Issues (Workflow User Interviews)](https://drive.google.com/file/d/1gSh9xoZPtU7NH0aIaYlCCaadc_oktUBW/view?usp=sharing)

2017-02 [Issues Filter](https://drive.google.com/file/d/1rRgwaLqtgfG1cFwjAWIyAtYvck2COumQ/view?usp=sharing)

2017-02 [Exploratory interviews regarding project management / issues.](https://gitlab.com/gitlab-org/ux-research/issues/2)

### Merge requests

2017-11 [Distinguishing between an issue and merge request](https://drive.google.com/file/d/1iuvAIaIp0yrTaBrU2yOWgZCoMhpQaq1o/view?usp=sharing)

2017-08 [Merge Requests (Workflow User Interviews)](https://drive.google.com/file/d/18907LrMTFhYrOZ2ZSa35002Jv772b38N/view?usp=sharing)

2017-07 [Merge request page](https://drive.google.com/file/d/1gPjP_CklY4RNZ5RClaAbY7p5msPr904I/view?usp=sharing)

### Navigation

2018-02 [Project & Group Level Tree Testing](https://drive.google.com/file/d/10DSXVTY610q2N7n-t2t5atpcwIjkyO2U/view?usp=sharing)

2017-12 [Project & Group Level Card Sorts](https://gitlab.com/gitlab-org/ux-research/issues/35)

2017-11 [Changing the sidebar color](https://drive.google.com/file/d/1HyV4wQRl3djZc8p-eNYVEuiI36WSJMrs/view?usp=sharing)

2017-04 [1.3 Global vs contextual navigation](https://gitlab.com/gitlab-org/ux-research/issues/7)

2017-04 [1.2 General navigation tasks - follow up testing](https://gitlab.com/gitlab-org/ux-research/issues/5)

2017-03 [1.1 General navigation tasks](https://gitlab.com/gitlab-org/ux-research/issues/3)

### Notifications (Todos, emails, realtime alerts, etc)

2018-05 [Should the "participants" feature be merged into a "subscribers" feature?](https://drive.google.com/file/d/1p8fn3WncvYRpX_7rsNaDyeSDki_i8qdR/view?usp=sharing)

2017-08 [Notifications (Workflow User Interviews)](https://drive.google.com/file/d/1-ZM0IsgkmOpmMTomKK62UNNhReMLY2p-/view?usp=sharing)

### Onboarding

2018-05 [Configuring SSH in GitLab](https://drive.google.com/file/d/14PG6O8eqOyyHZtUQ3DRwXNu5s0yzamwv/)

2017-11 [User feedback received via email (D.P)](https://drive.google.com/file/d/1k1-WqUG6EPR6TeQYHDQLhrCBh9tkh7EP/view?usp=sharing)

2017-08 [Challenges with onboarding new users (Workflow User Interviews)](https://drive.google.com/file/d/15Bf1nbunsRpKmVBeTNa1eY7mKvUnIDT-/view?usp=sharing)

### Projects

2017-11 [Creating a new project](https://drive.google.com/file/d/1Sj7N4hYxu27ud8KmIjyXwZwMsZCzSeOK/view?usp=sharing)

2017-08 [Projects (Workflow User Interviews)](https://drive.google.com/file/d/1egHFZYlardIeb7wD3cim1QKB3w2-vI-V/view?usp=sharing)

### Wiki

2017-08 [Wiki (Workflow User Interviews)](https://drive.google.com/file/d/1r202oWom6Zxp3_uGMUDGXgEPzO6mRJol/view?usp=sharing)

---

## Summary of "Dashboards" Research
   All data from studies related to the assessment and improvement of GitLab's dashboard design.

**Related Studies**

#### 2017-08 Dashboards - Workflow User Interviews - Results

Issue: https://gitlab.com/gitlab-org/ux-research/issues/24 

This research was conducted as part of a series of "Workflow Interviews" that investigated the different ways users interact with GitLab features. The interviews from this study focused on understanding users' expectations around using "Dashboards" and finding actionable ways to improve the design.

This research answered the following questions:

- What dashboards do people use?

- What information do users want on a dashboard?

- How are the above questions impacted by a person's job role, the project methodology they use, size of organization, etc?

Methodology: User interviews 

Key Findings:

- User 1: 'High up management' would like a dashboard that tells them: the total amount of issues within GitLab, the number of issues closed last week, how well the team is performing in releases, what version those releases are running. User 1 would like to share the 'high up management' dashboard on a large TV screen, so it's visible at all times and to anybody. A 'high up management' dashboard would give him a better sense of where things are up to on a particular project. On his personal Dashboard, User 1 wants to see what every team member has assigned to them (both issues and MRs) and needs to be able to view this cumulatively across all projects and on a per project basis.

- User 2: Explained that he has activity view set as default when he logs into GitLab. He has been a driving force in getting his company to adopt GitLab. User 2 uses the activity view to see if his colleagues are actively using GitLab. After viewing the Activity feed, User 2 checks his merge requests and then starts working in terminal. When returning to work on a project, User 2 usually looks at merge requests and pipelines. User 2 commented that his colleagues have kept the Project view as default when logging into GitLab.

- User 3: Uses a Google Spreadsheet to keep track of what every developer is working on. The spreadsheet consists of the GitLab issue URL, the title of the issue, estimated time to complete in days, assignee, priority and status. In order to create the spreadsheet, the user exported all issues from GitLab. The spreadsheet is being managed manually. The user explained that by using the spreadsheet it is easier for him to see who should work on what issue. He would like to see a similar view within GitLab.

- User 4: Despite User 4’s management preferring to manage issue via spreadsheet (see https://gitlab.com/gitlab-org/ux-research/issues/27), he stated a dashboard may replace their need for exporting issues to a spreadsheet. He explained that management want to be able to see everything at a glance, all on one page. When asked what information management would like to see on a dashboard, the user replied: how many issues are open/closed, in the todo column and in the doing column. He continued that graphs would be nice and he’d expect them to show trend analysis, default analysis, etc.

- User 5: Would like to see "everything that happened whilst I was away" on his personal dashboard. It is important to him that he can customize what type of activity he sees (issues, merge requests, etc), on a per repo basis. Stated that the Activity view within GitLab provided him with "way too much information" and that he couldn't find a logical way to filter it. User 5 would also like some basic statistics on what has happened in the last week, for example, how many MRs have been opened, how many issues have moved from one board to another and how many issues were created.

--

## Summary of "DevOps" Research
   All data from studies related to DevOps.

**Related Studies**

#### 2018-03 DevOps Insights: Operations Report & User Interviews

Issue: https://gitlab.com/gitlab-org/ux-research/issues/56

As part of the [Q1 2018 UX OKR](https://gitlab.com/groups/gitlab-org/-/epics/47) to: 'Establish Operations Engineers as a first class citizen' a survey was conducted to test the following assumptions:

- Operations Engineers focus on keeping things running smoothly and efficiently. They take the output of development and run it in production. With DevOps movement, they are trying to talk more closely with developers.

- Operations Engineers aren't different people. At many organizations, DevOps means one person does both development and operations. You wrote it, you operate it, you carry the pager.

Methodology: Survey research, User interviews 

Key Findings:

- Results showed that the second statement was found to be most true.

- We also found that within small organizations (less than 100 employees) respondents responsible for undertaking DevOps tasks identified as a Software Engineer. In contrast, respondents working for mid to large organizations (101+ employees) identified as a DevOps Engineer in addition to Software Engineer. Operations Engineer as a job title was not as popular as DevOps or Software Engineer.

- The top challenges being faced by Operations Engineers were rooted in the following categories: Lack of automation, tools, resource, and culture.

- [Survey report](https://drive.google.com/file/d/1sgPETOKc53QBLvd438gPljRRjeBOm8jr/)

- [User interviews report](https://drive.google.com/file/d/18r1cU8NVxFEHcI6RaJEQ_c-_-7SCNpAr/)

--

## Summary of "Issues" Research
All data from studies related to the assessment and improvement of GitLab's Issues design. 

**Related Studies**

#### 2018-01 Issue display & sort order

Issue: https://gitlab.com/gitlab-org/ux-research/issues/39 

Survey research that involved users answering questions about GitLab's Issues Boards and Issues Lists. This survey included users who were and were not using Issues.

Methodology: Survey research

Key Findings: 

* 71% of respondents stated that their preferred sort order for the 'Closed' column was recently closed items appearing at the top of the column.

* There was very little difference in what users wanted to see on a card in relation to the GitLab product they are using. GitLab.com users were more likely to want to see whether the issue had a related todo for them (50%) in comparison to self-hosted and/or paid GitLab instances. Enterprise users also had a slightly higher dependency on assignees (85%) and labels (77%).

* 26% of users who answered the survey did not use GitLab issues. Of those users, 31% stated that their primary reason for not doing so was because they were 'happy with their current issue solution/provider.' In the majority of cases (67%) users' current issue solution/provider was JIRA.

--

#### 2017-11 Distinguishing between an issue and merge request; Moving an issue between projects 

Issue: https://gitlab.com/gitlab-org/ux-research/issues/32

Usabilities studies that answered the following questions:
* Do users know how to move an issue between projects? (https://gitlab.com/gitlab-org/gitlab-ce/issues/34261)

* Is it easy to distinguish between an issue and merge request? (https://gitlab.com/gitlab-org/gitlab-ce/issues/38454)

Methodology: Click testing 

Key Findings:

* **Moving issues between projects:** The implementation of the 'Move Issue' CTA is still relatively new and users might still be getting to grips with its location. Potential need to test another location for the 'Move Issue' CTA. Open issue for retest can be found [here](https://gitlab.com/gitlab-org/ux-research/issues/37). 

* **Distinguishing between Issues and Merge Requests:** Whilst the majority of users could correctly identify what they were looking at, the results were quite low and could be improved upon. It took users almost 30 seconds to identify what they were looking at in each use case. 

--

#### 2017-08 Issues (Workflow User Interviews) 

Issue: https://gitlab.com/gitlab-org/ux-research/issues/27

This research was conducted as part of a series of "Workflow Interviews" that investigated the different ways users interact with GitLab features. The interviews from this study focused on understanding how users were using "Issues" and finding actionable ways to improve the design.

Methodology: User interviews 

Key Findings:

- User 1: Doesn't like the fact that he can't view all issues on one screen.

- User 2: User 2 isn’t using GitLab issues. Instead, issues are managed in Redmine which has been integrated with GitLab. At the time of adopting GitLab, GitLab didn't have any time tracking features. Therefore, it was unsuitable for the user’s company to utilize it for issue management. When later questioned about why they weren’t using GitLab issues now that time tracking was available, he explained that convincing management to move away from Redmine and to try something new was "difficult".

- User 3: Would like to see an issue’s due date from the board view. This is a feature which is available within Trello. User 3 is utilizing checklists to build issues which are ‘epics’, rather than user stories. Would like the issue description to be a wiki as he wants to see what changes have been made to an issue description (essentially version control for issue descriptions). He comments that Redmine has similar functionality.

- User 4: On the issues list view, the user would like to see absolute rather than relative timestamps. This has previously been mentioned by other users during usability testing but was in relation to MRs (https://gitlab.com/gitlab-org/ux-research/issues/21). User 4 uses issue boards. His board consists of the following columns: Backlog, To Do, Doing, Testing and Closed. The board is reviewed weekly in a meeting with the whole team (developers, testers and management).

- User 5: Wants an issues board that provides a global view of progress across all projects, rather than progress at a per project level. Expressed frustration at assigning and removing labels. A typical issue has 80-100 labels assigned to it. It's very difficult for them to select the right labels from the dropdown menu and it isn't intuitive how to remove a label. Pivotal Tracker was able to effectively handle issues with a high number of labels.

--

#### 2017-02 Issues Filter

Issue: https://gitlab.com/gitlab-org/ux-research/issues/1

This research identified usability issues related to the implementation of the Issue filter, by testing the following hypotheses:

- Users will want to multi-select labels using their mouse.

- Users will not instinctively enter a space between labels to trigger the top-level filter options.

- Users will want to clear a selected label with one click.

- Users will not know that they can search for a label by typing.

- Users will use abbreviated words when searching within a dropdown. Subsequently, they will be frustrated at the accuracy of their results.

- Users will want the labels they have selected in the issue filter to be visually represented with color.

Methodology: Usability testing 

Key Findings:

- **Users will want to multi-select labels using their mouse** (https://gitlab.com/gitlab-org/gitlab-ce/issues/26670). 2 out of 5 users attempted to multi-select labels using their mouse.

- **Users will not instinctively enter a space between labels to trigger the top-level filter options** (https://gitlab.com/gitlab-org/gitlab-ce/issues/26670). 2 out of 5 users did not instinctively enter a space between labels to trigger the top-level filter options. 

- **Users will want to clear a selected label with one click.** No users requested this functionality. Generally, users were happy to select the text, delete it and hit it.

- **Users will not know that they can search for a label by typing.** 3 users recognized that they can search for a label by typing. 

- **Users will use abbreviated words when searching within a dropdown. Subsequently, they will be frustrated at the accuracy of their results.** No users used abbreviated words when searching within a dropdown. Generally, their initial behavior within a dropdown was to scroll, followed by searching for the full word length.

- **Users will want the labels they have selected in the issue filter to be visually represented with color.** No users mentioned the fact that labels were not visually represented with color within the issue filter, when they were prompted to suggest improvements to the filter.

--

#### 2017-02 Exploratory interviews regarding project management / issues.

Issue: https://gitlab.com/gitlab-org/ux-research/issues/2

This research study answered the following questions:

- How are users using issue boards currently? How can we improve expectations?

- What do users care about with regard to team-collaboration? How can issue boards move in this direction? https://gitlab.com/gitlab-org/gitlab-ee/issues/1295.

- Is this the best approach? Any alternatives? Should group labels be used for group-level issue boards? https://gitlab.com/gitlab-org/gitlab-ee/issues/928

- How are users using GitLab as a whole? In regards to single repo use cases versus team-first use cases? How do we better integrate issues and issue boards together?

Methodology: User interviews 

Key Findings:

While the main aim of the interviews was to explore how users manage issues boards, the following points were discussed and raised by users:

- Ability to manage multiple projects on one issues board. (https://gitlab.com/gitlab-org/gitlab-ee/issues/928, https://gitlab.com/gitlab-org/gitlab-ee/issues/1295.)

- Ability to add tasks easily with little detail. (https://gitlab.com/gitlab-org/gitlab-ce/issues/28539, https://gitlab.com/gitlab-org/gitlab-ce/issues/27829)

- An issues board which has the simplicity of a physical board and allows them to see at a glance what issues are being worked on. (https://gitlab.com/gitlab-org/gitlab-ce/issues/28542)

- An issues board which has swimlanes. (https://gitlab.com/gitlab-org/gitlab-ce/issues/28536)

- An issues board which shows interdependencies / relationships between issues (such as parent/child, subject/story, etc). (https://gitlab.com/gitlab-org/gitlab-ce/issues/21453)

- When viewing a digital project board, a developer only wants to view issues which are assigned to their self. (https://gitlab.com/gitlab-org/gitlab-ce/issues/28537)

- Ability to auto-assign specific fields as an issue is created, especially if the issue is raised through a third party ticketing system. Accompanied by the ability to search/query these custom fields. (https://gitlab.com/gitlab-org/gitlab-ce/issues/8988 , https://gitlab.com/gitlab-org/gitlab-ce/issues/28554)

- Ability to automatically import issues into GitLab from third party tools. (https://gitlab.com/gitlab-org/gitlab-ce/issues/12631, https://gitlab.com/gitlab-org/gitlab-ce/issues/23530, https://gitlab.com/gitlab-org/gitlab-ce/issues/23929)

- To know how long an issue has remained in a column without moving / tracking the time taken for each workflow transition '(semi-related to: https://gitlab.com/gitlab-org/gitlab-ce/issues/28448. Might be covered by Chris Peressini's pending issue for combining board stages into cycle analytics)

--

## Summary of "Merge Requests" Research
All data from studies related to the assessment and improvement of GitLab's Merge Requests design.

**Related Studies**

#### 2017-11 Distinguishing between an issue and merge request

Issue: https://gitlab.com/gitlab-org/ux-research/issues/32

Usabilities studies that answered the following question:

- Is it easy to distinguish between an issue and merge request? (https://gitlab.com/gitlab-org/gitlab-ce/issues/38454)

Methodology: Click testing

Key Findings:

- **Distinguishing between Issues and Merge Requests:** While the majority of users could correctly identify what they were looking at, the results were quite low and could be improved upon. It took users almost 30 seconds to identify what they were looking at in each use case.

--

#### 2017-08 Merge Requests (Workflow User Interviews)

Issue: https://gitlab.com/gitlab-org/ux-research/issues/29

This research was conducted as part of a series of "Workflow Interviews" that investigated the different ways users interact with GitLab features. The interviews from this study focused on understanding how users were using "Merge Requests" and finding actionable ways to improve the design.

Methodology: User interviews 

Key Findings:
- User 2: Normally checks out the Changes tab on a merge request and views/comments on the diff whereas some of his colleagues prefer to check Commits and then use the Discussions tab to leave a comment. As his colleagues haven’t checked out the diff, they don’t always see the latest changes to the MR and there have been occasions where they have commented on something which has already been fixed. Doesn’t use the Jobs view and prefers the Pipelines screen as he feels it’s faster for him to navigate to the information he needs in comparison.

- User 3: Explains that developers normally share their screen and demonstrate the changes they have made, before a merge request is accepted.

- User 5: Would like to see a global overview (board view) of all MRs across all projects. Wants a more "official way" to assign someone to review a merge request. Referred to GitHub's review [request feature](https://github.com/blog/2291-introducing-review-requests).

--

#### 2017-07 Merge request page

Issue: https://gitlab.com/gitlab-org/ux-research/issues/21

Usability studies that involved the testing of [Prototype A](https://framer.cloud/uapjR/index.html) and [Prototype B](https://framer.cloud/zSLHJ/index.html) to answer the following questions:

- What are users' current pain points with the existing layout of a merge request page? _(For example, is it frustrating locating the MR widget and the Discussion/Commits/Pipelines/Changes tabs on MRs with long descriptions?)_

- Are the MR's branch details clear?

- Is the status of the MR obvious?

Methodology: Usability testing, Prototyping  

Key Findings: 

- **Branch name** - Prototype B had a higher success rate and satisfaction score. Users were also much faster to locate the branch details on Prototype B. 

- **Description expansion** - There was a split in user opinion and generally, this seemed related to the type of workflow a user had. Users who work at pace, opening and closing MRs within a few days, wanted the description to be truncated. This is because they already had a good idea about what the MR was about and didn't typically re-read the description. Users who reviewed MRs which spanned weeks or even months, wanted the full MR description visible at all times, as they would often refer to it when revisiting the MR. Manually expanding the description is obstructive to their workflow. 

- **MR status tab moved to first position / open as default.** - When users were asked what information they would look for first when reviewing a MR, the top answer was 'diffs' as users want to understand what changes have been made to the MR. The Discussion tab was more popular since users wanted to know what comments, if any, had been made by their colleagues. Users who were used to GitLab's existing experience, accidentally visited the Merge Status tab when trying to view Discussions, which caused some frustration. The users who did prefer the Merge Status tab as default were typically working solo or in a very small team and therefore didn't utilize the Discussion tab, hence their preference for the Merge Status tab to be default instead.

## Summary of "Navigation" Research
All data from studies related to the assessment and improvement of GitLab's navigation.

**Related Studies**

#### 2018-02 Project & Group Level Tree Testing

Issue: https://gitlab.com/gitlab-org/ux-research/issues/43

This research tested the following questions:

- How much of an improvement is the recommended structure for the contextual navigation (as detailed in #35)?
- Do the differences in how content is organized at a project and a group level cause users confusion?
- Are there any further improvements we can make to the recommended structure based on the results of the tree test?
- Do users confuse the content of `Graph` and `Charts`?
- What small, iterative changes can we make to the sidebar navigation?

Methodology: Tree Testing

Key Findings:

- **Recommendation 1** At a project level, change: Overview/Details to Project/Details
   - +7% increase in users successfully completing the task.
   - In comparison to the existing structure, users were +3 seconds quicker to complete the second task they were served. Emphasizing the differences at a project and group level have helped users to identify where they are located within GitLab.

- **Recommendation 2**: At a group level, move Issues/Milestones to Planning/Milestones
   - +5% increase in users successfully completing the task.
   - 26% of users who completed tasks on the existing tree test structure incorrectly selected the Epics List or Roadmap as their answer, highlighting that user intent is to see Epics, Roadmap and Milestones grouped together (or perhaps even consolidated into one view). Users did not typically look for this grouping within the category of Issues.

- **Recommendation 3**: At a project level, move Repository/Charts to Statistics/Charts
   - Users who completed tasks on the existing structure had more experience of using Charts (Proposed:32% vs Existing:46%), despite this the results for both success (Proposed:53% vs Existing:54%) and time taken (Proposed:13 seconds vs Existing:12 seconds) were almost identical. We need more data to understand the true value of moving Charts to Statistics, however, we've identified that it's a low-risk change to release and it would make sense to measure the impact post-release.

- **Recommendation 4**: At a project level, move Overview/Cycle Analytics to Statistics/Analytics
   - +5% increase in users successfully completing the task.

- **Recommendation 5**: At a group level, when released, add Roadmap to Planning/Roadmap. At a group level, move Epics/List to Planning/Epics.
   - Users had a higher success rate when completing the Roadmap task on the proposed structure (+4% increase in success).
   - Epics performed poorly on both the proposed and existing structure, however, this is to be expected as the accompanying questionnaire to the tree tests showed that only a small number of users were utilizing this functionality (Epics had been released for approx 1 month when the testing took place). Settings was the most common answer on both tree tests. This may potentially indicate that users feel they need to toggle on Epics in order to view them within GitLab.

--

#### 2017-12 Project & Group Level Card Sorts

Issue: https://gitlab.com/gitlab-org/ux-research/issues/35

This research answered the following questions:

- Is `Charts` misplaced under `Repository`? Would a more suitable location be within an `Insights` category?
- Is `Contributors` misplaced under `Repository`?
- Should `Labels` appear in the top level navigation at a **project** level?
- Should `Labels` appear in the top level navigation at a **group** level?
- Should `Milestones` appear in the top level navigation at a **project** level?
- Should `Milestones` appear in the top level navigation at a **group** level?
- Should `Epics` appear in the top level navigation at a **group** level?
- Are there differences in the way users sorted cards at a project and group level?
- What is the optimal structure of the contextual navigation at a project and group level?

Methodology: Card sorting 

Key Findings:

- `Charts` is misplaced under `Repository`. Users felt `Charts` should appear within `Statistics`.
- `Contributors` is misplaced under `Repository`. Users felt `Contributors` should appear within `Statistics`.

- `Labels` was never categorized on its own as a top-level navigation item at a project level. 61% of users grouped `Labels` with other content found under the heading of `Issues`. The alternative suggestion of moving `Labels` to `Merge Requests` (mentioned in https://gitlab.com/gitlab-org/gitlab-ce/issues/39268) is no longer possible as `Merge Requests` is no longer a top-level category. 

- `Labels` was never categorized on its own as a top-level navigation item at a group level. 55% of users grouped `Labels` with other content found under the heading of `Issues`.

- `Milestones` was never categorized on its own as a top-level navigation item at a project level. 67% of users grouped `Milestones` with other content found under the heading of `Issues`.

- `Milestones` was never categorized on its own as a top-level navigation item at a group level. 72% of users grouped `Milestones` with other content found under the heading of `Planning`.

- `Epics` was never categorized on its own as a top-level navigation item at a group level. 69% of users grouped `Epics` with other content found under the heading of `Planning`.

--

#### 2017-11 Changing the sidebar color

Issue: https://gitlab.com/gitlab-org/ux-research/issues/32

This research answered the following question:

- What impact will changing the color of the sidebar have on a user's ability to find content located in the sidebar? Related CE issue: https://gitlab.com/gitlab-org/gitlab-ce/issues/35740

Methodology: Click testing 

Key Findings: 

- A white sidebar performed worse than our existing sidebar colour. Users also had a higher propensity to click the ‘Edit’ CTA on the issue description when they were shown the white sidebar, suggesting that users were less likely to align the sidebar content with the page content.

--

#### 2017-04 1.3 Global vs contextual navigation

Issue: https://gitlab.com/gitlab-org/ux-research/issues/7

Follow-up testing to: [ux-research#3](https://gitlab.com/gitlab-org/ux-research/issues/3) and [ux-research#5]https://gitlab.com/gitlab-org/ux-research/issues/5. Related to: [ux-research#6]https://gitlab.com/gitlab-org/ux-research/issues/6.

The purpose of this research was to answer the following questions:

- Is the user aware of their location (breadcrumb) when in a very deep level? Can they understand the steps from the top-most level to the level they’re currently in?
- Can the user recognize what group/subgroups a project is in? Also, how many?
- Can the user navigate to the subgroup containing the project?
- Is the user aware and understands the subgroups within the space they are viewing (breadcrumb exploration)?
- Can the user navigate to and within their personal space?
- Can the user explore projects on the instance and navigate to them?
- Does the user understand the difference between the list of projects they have access to (“Your projects”) and the list of projects inside of a group?
- Can the user easily find and identify secondary menu items within a contextual navigation?

Methodology: Usability testing

Key Findings:

- Users who interacted with Prototype A took almost twice as long to complete task 6 which was to locate issues. This was the first task that asked users to interact with the contextual navigation at a project level. This suggests that users of Prototype A may have been slightly more disorientated than users of Prototype B at this point during the testing. However, later tasks which asked users to find other content within the contextual navigation at a project level show that Prototype A and B performed similarly. Indicating that users of Prototype A became more familiar with the contextual navigation through use and ended up with a comparable understanding to users testing Prototype B.

- Users of Prototype A also took almost as twice as long to complete task 7 which was to locate the issues icon, although overall users of Prototype A generally had a greater success rate of completing this task. This was the first task that asked users to interact with their personal navigation. Despite Prototype A having a higher completion rate, the task time was affected due to more users taking an indirect path to the issues icon. Whereas the majority of users interacting with Prototype B who did successfully complete the task took a direct path. This indicates that users of Prototype A were confused about the content they could find within the contextual navigation. This correlates with the email feedback from one user (who used Prototype A) in relation to `+ icon`, which he also expected to find within the contextual navigation:

- Users of Prototype B took over twice as long to complete task 12, which was to find how many subgroups the group of 'customers' had. This is because users of Prototype A had by this point in the testing a much greater understanding of what they could expect to find in the global navigation vs contextual navigation. Almost all users of Prototype A took a direct path to their subgroups and did so via the global navigation, resulting in a faster task completion time. Users of Prototype B took varied, indirect paths.

--

#### 2017-04 1.2 General navigation tasks - follow up testing

Issue: https://gitlab.com/gitlab-org/ux-research/issues/5

Follow-up testing to: [ux-research#3](https://gitlab.com/gitlab-org/ux-research/issues/3). Related to: [ux-research#4](https://gitlab.com/gitlab-org/ux-research/issues/4)

This research tested the following issues:

- [Make Secondary Navigation elements available on hover](https://gitlab.com/gitlab-org/gitlab-ce/issues/22069)

- [Make 'Projects' page clearer and easier to see own projects](https://gitlab.com/gitlab-org/gitlab-ce/issues/29045)

- [Add badge count to project/group tabs](https://gitlab.com/gitlab-org/gitlab-ce/issues/29798)

- [Create Todo from collapsed sidebar + new todo icon](https://gitlab.com/gitlab-org/gitlab-ce/issues/24805)

- [Viewing counts of and navigating to global dashboard issues and merge requests](https://gitlab.com/gitlab-org/gitlab-ce/issues/29866)

- [Inconsistent location of members page between groups and projects](https://gitlab.com/gitlab-org/gitlab-ce/issues/29893)

- [Extra functionality for the top right Plus Button](https://gitlab.com/gitlab-org/gitlab-ce/issues/23603)

Methodology: Usability testing 

Key Findings: 

- Personal projects: All users were able to find their personal projects. [gitlab-ce#29045](https://gitlab.com/gitlab-org/gitlab-ce/issues/29045) 

- Issues and MRs: Despite some confusion about what the issues icon represents, once users had clicked the icon and viewed the content, they generally understood that these were their personal issues. Similarly, the majority of users who clicked the MR icon understood that the MRs displayed were relevant to them. The new icons are better than the current sidebar at conveying to users that the content is personal to them. [gitlab-ce#29866](https://gitlab.com/gitlab-org/gitlab-ce/issues/29866) 

- Issues board: The ‘issues board’ task had an improved success rate due to secondary level items appearing on hover of primary level items. [gitlab-ce#22069](https://gitlab.com/gitlab-org/gitlab-ce/issues/22069) 

- Tanuki: Increase in interaction with the tanuki. Users commented that they expected the tanuki to take them ‘home’ to their personal dashboard. Users expected to see the following on their personal dashboard: issues (both issues I’m assigned to and watching), merge requests, projects and recent activity. Basically “everything associated with me”.

- Number of projects: No users manually counted their projects, following the addition of the badge count to project/group/subgroup tabs. [gitlab-ce#29798](https://gitlab.com/gitlab-org/gitlab-ce/issues/29798)

--

#### 2017-03 1.1 General navigation tasks

Issue: https://gitlab.com/gitlab-org/ux-research/issues/3

This research tested the various scenarios that users may encounter when navigating through GitLab.

Methodology: Usability testing 

Key Findings:

- Project navigation: It isn’t immediately obvious to users that there are two levels of project navigation. This is, in large, due to the fact that users have to click the primary level of project navigation, in order to view the second level of project navigation. Even when guided, some users still overlooked the second level of navigation, scrolling past it to see the main body of content on a page. **UX READY: https://gitlab.com/gitlab-org/gitlab-ce/issues/22069**

- Group navigation: When creating a new project within a group, users often thought they needed to be located within the group they were asked to add the project to, before they proceeded to click ‘New Project’. **Combined with https://gitlab.com/gitlab-org/gitlab-ce/issues/23603**

- Sidebar: There were two extremes of user behavior when interacting with the sidebar: 1) Some users didn't instinctively interact with the sidebar as it isn’t open by default. For example, it took User 3 9 minutes to spot there was a menu available. Those that did find the sidebar easily, typically proceeded to treat it as their primary method of navigation during the testing. Subsequently, this meant they often failed tasks where the content couldn't be found within the sidebar. **Combined with https://gitlab.com/gitlab-org/gitlab-ce/issues/29878**

- Global Navigation: Users do not understand what the ‘Todos’ icon represents. It was mistaken for “unread messages” and “notifications”, not a task list which the user needs to action. This was further emphasized when one user remarked that once he had viewed an item, he expected it to automatically reduce his number of ‘Todos’. He didn’t understand why he needed to click ‘Done’. **OPEN MR: https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/7827**

- Search: Users expect to be able to multi-select items from the menu that appears when the search bar is clicked in i.e. ‘Issues assigned to me’ AND ‘Merge requests assigned to me’ (rather than OR). Users expect to find help topics within the search bar. **https://gitlab.com/gitlab-org/gitlab-ce/issues/29898**

- Preferences: Users got confused between their ‘Default Dashboard’ view and default ‘Project View’. **https://gitlab.com/gitlab-org/gitlab-ce/issues/29887**

- Other (Todos): Users do not realise that ‘To dos’ are personal to them. For example, User 2 thought it was what each team was working on. It isn’t immediately obvious to users that the ‘Todos’ page contains a mixture of issues, merge requests and @ mentions unless prompted.

--

## Summary of "Notifications" Research
All data from studies related to the assessment and improvement of GitLab's Notifications design.

**Related Studies**

#### 2018-04 Should the "participants" feature be merged into a "subscribers" feature?

Issue: https://gitlab.com/gitlab-org/ux-research/issues/61

The purpose of this research was to evaluate the feature proposal outlined in [gitlab-ce#45211](https://gitlab.com/gitlab-org/gitlab-ce/issues/45211), by gaining a better understanding of the expectations users have around being listed as a "Participant" in an issue and enabling notifications.

Methodology: Survey research 

Key Findings:
- For most respondents, if they're actively participating in the issue, it doesn't make a difference if others know that they have turned on notifications since they feel that their participation is public already. Privacy is mainly an issue for situations where users are not actively participating, such as when they'd just like to keep tabs on a discussion or track the progress of an issue. In this case, many respondents would like the option to enable notifications privately.

    - 81% of respondents stated that they turn on notifications for issues they are neither working on nor actively discussing.

- 52% of respondents agreed/strongly agreed with the feature proposal, 28% disagreed/strongly disagreed, and 19% neither agreed nor disagreed. Expectations around choice and privacy could shape the way that users react to this change.

    - About 73% (36 out of 49) of the respondents who agreed/strongly agreed with the proposal previously stated that they _do not mind_ if the public is aware that they have turned on notifications.

    - On the other hand, about 56% (15 out of 27) of the respondents who disagreed/strongly disagreed with the proposal previously stated that they prefer to turn on notifications _privately_.

    - These findings may indicate that the “subscribers” feature could be welcomed, if users are still given the option to subscribe privately to an issue. [Click here](https://docs.google.com/document/d/1Mf2dio5ifm7oPHZUMktrDYQvw9ODfaYIon4ZgVYdilo/edit?usp=sharing) to read the full feedback on the proposal from respondents.
 
    - Some respondents questioned whether it is more useful to know who is _involved in or working on_ an issue rather than knowing who is receiving notifications. For this reason, some respondents felt that the two concepts should stay separate.

- Some respondents feel that subscribing to notifications should be by choice, and that being mentioned in an issue shouldn’t automatically subscribe you.

--

#### 2017-08 Notifications (Workflow User Interviews)

Issue: https://gitlab.com/gitlab-org/ux-research/issues/25

This research was conducted as part of a series of "Workflow Interviews" that investigated the different ways users interact with GitLab features. The interviews from this study focused on understanding how users were managing notifications and finding actionable ways to improve the design.

Methodology: User interviews 

Key Findings: 

- User 1: Doesn't use todos as he's tagged into too many things. Visits projects once a week to tidy them up: getting rid of redundant branches that haven't been deleted, to close issues or write a description in an MR that closes an issue. He has event emails set up from GitLab which notify him about issues, merge requests and when a pipeline fails. None of the emails sit in his primary inbox, they are all filtered into individuals folders within a master GitLab folder in his inbox. Doesn't mind push notifications / realtime alerts appearing momentarily on screen but he guarantees that he would close them and he definitely would mute them on a mobile device. He feels that he'd receive too many alerts for the number of projects he has and it would be far too many for him to action.

- User 2: Tried using todos but has never used todos to manage MRs (predominantly what his company is using GitLab for, along with CI). Gets event emails from GitLab (configured within Slack) but he doesn’t typically read them as the information is outdated and he’s already read about it in Slack. Wouldn’t mind receiving alerts when he has a new merge request. Would like to apply the same Slack settings for all repos in one group. 

- User 3: When asked whether he was using Todos, User 3 went to an issue and showed a checklist he had created within an issue. He does not use todos but mentioned that some of the development team use this view. However, developers are unable to understand what their priorities are and what they should work on next by just looking at Todos. Subsequently, they normally need to double check what they should be working on with User 3. Receives event emails from GitLab but hardly ever checks them. User 3’s team communicate via Skype, therefore he typically receives notifications/progress updates from his colleagues. He doesn’t rely on GitLab’s event emails to update him on the progress of an issue, MR, etc.

- User 4: Doesn’t use todos. User 4 doesn’t have event emails from GitLab enabled, although he would like to enable them but just hasn’t had the time. He wasn’t sure what events GitLab could email him about but stated that he would want to be notified: when a branch is pushed, when an issue/MR is assigned, when an issue is created and when a milestone was approaching for a particular issue. When asked about realtime alerts, the user said he wanted to set up emails. He didn't seem interested in receiving alerts within GitLab's interface.

- User 5: Utilizing todos. He has integrated Flowdock with GitLab so GitLab events are pushed into Flowdock. Normally starts his day by checking event emails from GitLab with the aim of actioning items and emptying his inbox. He stated that he doesn't like email and would much prefer to see real-time updates within GitLab's interface. After checking his email, he checks his todos, usually to see if anyone wants him to review their merge request. He doesn't like the fact that todos automatically disappear and would prefer to explicitly mark them as done. User 5 further added that he would have less reliance on emails if he was notified about events within GitLab's interface, especially if he could create a todo from a notification. 

--

## Summary of "Onboarding" Research
All data from studies related to the assessment and improvement of GitLab's onboarding experience.

**Related Studies**

#### 2018-04 Configuring SSH in GitLab

Issue: https://gitlab.com/gitlab-org/ux-research/issues/53

Configuring SSH is a notoriously hard thing to do, as reflected by the fact that 'ssh' has been one of the most searched terms in GitLab's documentation. The aim of this research was to identify a number of improvements that would make it easier for people to add their SSH key to GitLab.

This research answered the following questions: 

- How and why are users struggling to add their ssh key?

- How can we make this easier?

Methodology: Unmoderated usability testing

Key Findings: 

- Problems with [SSH instructions:](https://gitlab.com/help/ssh/README)
  - It isn't immediately obvious to users that commands can be copied and pasted.
  - It isn't always obvious to users what part of a command (e.g. file path or name) may need to be altered to suit their personal circumstances.

- Problems that arose in Terminal: 
  - Users demonstrated hesitation when asked to 'Enter file in which to save a key'. As users have now moved into Terminal, they follow the instructions outlined within Terminal rather than referring back to the instructions within GitLab. This can cause confusion if the user attempts to change the file name.
  - User thought he needed Git installed to generate an SSH key

--

#### 2017-10 User feedback received via email (D.P)

Issue: https://gitlab.com/gitlab-org/ux-research/issues/34

User feedback collected from Hacker News and email, in regards to the usability of GitLab's UI including comments on GitLab's whitespace, typography, and colors.

Key Findings:

- Improvements to initial screen: https://gitlab.com/gitlab-org/gitlab-ee/issues/1869, https://gitlab.com/gitlab-org/gitlab-ee/issues/1869

- Further improvement to new project page: https://gitlab.com/gitlab-org/gitlab-ce/issues/38895 

- Improvement to import screen: https://gitlab.com/gitlab-org/gitlab-ce/issues/14010

- Improvement to imports that have no repo: https://gitlab.com/gitlab-org/gitlab-ce/issues/39613

- Getting started with CI/CD: https://gitlab.com/gitlab-org/gitlab-ce/issues/37043

- Additional issues: https://gitlab.com/gitlab-org/gitlab-ce/issues/28614,
https://gitlab.com/gitlab-org/gitlab-ce/issues/24310, and https://gitlab.com/gitlab-org/gitlab-design/issues/26.

--

#### 2017-08 Challenges with onboarding new users - Workflow User Interviews - Results

Issue: https://gitlab.com/gitlab-org/ux-research/issues/28

This research was conducted as part of a series of "Workflow Interviews" that investigated the different ways users interact with GitLab features. The interviews from this study focused on understanding users' onboarding experience and finding actionable ways to improve the design.

Methodology: User interviews 

Key Findings: 
- User 1: Tells developers not to edit/write code in GitLab. He advises them to pull down a local copy and use command line tools. There have been some challenges with people editing files within GitLab, which they didn't realise they were editing like ReadMeFiles. The recent change to merge requests which deleted branches by default was a problem. User 1 explained that developers had begun to get used to a certain workflow and when that changed, they didn’t realise. It took him a whole day of going back and opening up previous commits with new branches. User 1 often instructs people how to use GitLab by email and he sometimes finds those emails are ignored.

- User 5: “It’s not easy to recognise where you are in GitLab”. User 5's colleagues are unable to instantly recognise whether they are looking at an issue or a merge request. User 5 commented that GitLab's interface looks too much alike everywhere. Previously, User 5's colleagues used GitHub for pull requests and Pivotal Tracker for issues, resulting in two very different, distinct interfaces, subsequently, making it much easier for them to recognise at a glance the type of content they were viewing.

- Around 100 users are non-technical. Non-technical users typically just want to manage/follow issues. Logging into GitLab is overwhelming for them. User 1 explained that non-technical users have to access a project to see issues, but by accessing a project they see code and that can be off-putting. A greater amount of user roles would be useful. In the case of User 1, a non-technical user just needs to see issues and the ReadMe file.

--

## Summary of "Projects" Research
All data from studies related to the assessment and improvement of GitLab's Project design.

**Related Studies**

#### 2017-11 Creating a new project

Issue: https://gitlab.com/gitlab-org/ux-research/issues/32

Usabilities studies that answered the following question:

- Is the new project page overwhelming? Are the tabs discoverable? Should we iterate towards Pedro's 'project flow' design? [(gitlab-ce#37399)](https://gitlab.com/gitlab-org/gitlab-ce/issues/37399)

Methodology: Click testing

Key Findings:

- **Project page design:** A tabbed design did not hinder discoverability although users were still overwhelmed by the options available on the 'Blank Project' tab, leading to lower accuracy and speed.  Subjects in the "Wizard" group faired best in both tasks.

--

#### 2017-08 Projects (Workflow User Interviews)

Issue: https://gitlab.com/gitlab-org/ux-research/issues/26

This research was conducted as part of a series of "Workflow Interviews" that investigated the different ways users interact with GitLab features. The interviews from this study focused on understanding how users were using "Projects" and finding actionable ways to improve the design.

Methodology: User interviews 

Key Findings
- User 1: 350 projects within GitLab. Working actively on 30-40 projects.

- User 2: 198 projects within GitLab. Working actively on around 20 projects.Using subgroups and groups to organize projects. Explained that he and another developer are working on the same project at the moment, so they’ve created a group for themselves which contains a repo for documentation and a repo for code. The group is restricted so that nobody else can see it. On average 1 new project per week is created. All developers and the System Administrator have the appropriate permissions to create new projects. Projects are never closed, they remain open.

- User 3: 15 projects within GitLab. All have been actively worked on within the last 6 months. 9 projects have been actively worked in the past week. Has set-up projects for each team (api, ui, web, core, etc) which are only used for source code. A master project is used for issue management. The details appertaining to all features are documented in issues within the master project. Explained that it was easier to keep track of the product vision when all issues are stored in the same location.

- User 4: Has 15 projects within GitLab. He is using subgroups and groups. He has created 3 groups: 1 group called Developers which contains all the software that runs on his company’s products, 1 group for Tools which contains utilities, scripts, support, etc (basically software which doesn’t run on the company’s products) and the last group is a sandbox environment where people can experiment with GitLab. As a GitLab admin, User 4 is in charge of creating projects. User 4 explained that projects are created when needed. He follows the guideline of if it runs on a separate piece of hardware/product then it needs a separate repository. If there are variations of a product, then he’ll use the same repo but create different branches for each variation. Uses the GitLab interface to create Projects and therefore automatically creates a ReadMe file for each project. The ReadMe contains information on how to access a project, add to it and where the output files are. The user only updates the ReadMe file after setting up and testing a project out locally.

- User 5: Has 30-40 projects within GitLab. He is actively working on 10 projects.

--

## Summary of "Wiki" Research
All data from studies related to the assessment and improvement of GitLab's Wiki design.

**Related Studies**

#### 2017-08 Wiki (Workflow User Interviews)

Issue: https://gitlab.com/gitlab-org/ux-research/issues/23

This research was conducted as part of a series of "Workflow Interviews" that investigated the different ways users interact with GitLab features. The interviews from this study focused on understanding how users were using the "Wiki" feature and finding actionable ways how to improve the design.

Methodology: User interviews

Key Findings:

- User 1: Uses wikis for internal documentation. Examples he gave included: Getting started with GitLab, workflows, coding standards, testing. He also uses wikis for project-specific information. Examples included: how to communicate with a project's API, what database structures exist within a project, what all of the answerable playbooks do within a project. Wikis are not his ideal solution, he explained that people don't understand the structure of wikis properly and he would prefer more of a "book style". For his personal projects, he uses GitLab Pages. He has wondered whether GitLab Pages would provide a better way of managing internal / project documentation. He concluded that although a "book style" approach to documentation may be easier to use, it would probably be more complex to contribute to. 

- User 2: Typically uses ReadMe files for project documentation. He had used ReadMe files to document C.I. User 2 explained that some of his colleagues prefer to use wikis within RedMine. Subsequently, some projects have documentation in two locations causing some duplication. When asked whether he’d used GitLab’s wiki, User 2 confirmed that he had, however, nobody else at his company had. This was due to the fact that the team were comfortable using Redmine and haven’t yet developed enough trust in GitLab to use all its features.

- User 3: A master project which is used for issue management (not source code) is the only project which utilizes GitLab’s wiki. The wiki includes the following topics: how to manage tickets, how to run and install projects, style guidelines, etc. Instead of adding and maintaining a ReadMe file for multiple projects, the wiki is used to document all information.

- User 4: Not using wikis.

- User 5: Isn't extensively using wikis. He has one main wiki which is used for internal documentation. Within the wiki he has documented the process of migrating to GitLab from GitHub and Pivotal Tracker. He has also explained how to use GitLab C.I. He has some repos that are static documentation sites and he has thought about converting them to wikis. However, he's not entirely sure if a wiki format is a suitable replacement. He likes to use wikis for "gotchas" - things that not everybody knows, general procedures and knowledge. He would prefer to use one wiki per project. However, he did state that similar information is sometimes stored within a ReadMe file so it's difficult to decide what information should be stored where. User 5 has also started exploring GitLab pages. 



