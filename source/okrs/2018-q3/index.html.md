---
layout: markdown_page
title: "2018 Q3 OKRs"
---

## On this page
{:.no_toc}

- TOC
{:toc}

### CEO: Grow Incremental ACV according to plan. IACV at 120% of plan, pipeline for Q4 at 3x IACV minus in quarter orders, LTV/CAC per campaign and type of customer.

* VPE
  * Support
    * Self-hosted: 95% on CSAT, 95% on Premium SLAs
    * Services: 95% on CSAT, 95% on Gold SLAs
* CMO
    * Growth Marketing: Achieve 3x IACV in Q4 pipeline minus in quarter orders
        * Content: Execute Cloud campaign to support demand generation. Produce 3 cloud native focused webinars with a total of 1000 registrations, publish 1 cloud native-focused case study.
        * Content: Execute Security campaign to support demand generation. Produce 3 security focused webinars with a total of 1000 registrations, publish 1 security-focused case study. 
        * Content: Increase organic search traffic to accelerate awareness of GitLab brand and product capabilities. Publish 10 feature tutorial posts, publish /devops-culture, publish /devops-tools. 
        * Field Marketing: Improve account based marketing for large & strategic accounts. Pull in 1 large or strategic deal into Q3, increase opportunity size of 2 large or strategic deals, 5 intros to new departments at large or strategic accounts. 
        * Field Marketing: Increase operational maturity in field marketing tracking and execution. All event lead uploads within 48 hours, all contacts accurate statuses and notes, accurate campaign data for all field marketing campaigns tracked in salesforce.com.
        * Online Growth: Expand online growth tactics. Increase overall traffic to existing pages by 10% compared to last quarter, increase sign-ups from SEO/PPC/Paid Social by 20% compared to last quarter . 
        * Online Growth: Build ad campaigns aligned to cloud and security themes. Double traffic to /cloud-native, 500 MQLs from online advertising.
        * Online Growth: Improve about.gitlab.com conversion rate optimization. Increase live chat leads 20%, increase contact requests by 20%, increase trial sign-ups by 20%.
        * Marketing Program Management: Launch new email nurture series educating trial requesters on .com Gold plan. Keep unsubscribe rate below 1%, 30% of nurture audience at "engaged" progression status or higher.
        * Marketing Program Management: Increase awareness and ongoing education on Gitlab <> Google Partnership. Increase GKE trials referred by Gitlab/week by 3X.
        * Marketing Program Management: Improve reporting for marketing programs. Automate and schedule reporting for email & webcast performance, ensure all email and webcast programs are tracked completely in salesforce.com campaigns. 
        * Marketing Operations: Streamline lead management processes. Refreshed lead & contact layouts with all unnecessary fields removed from view, trim down to a single lead queue view for SCA team.
        * Marketing Operations: Deploy multi-touch attribution model in Bizible.
        * Marketing Operations: Ensure all online marketing is tracked as campaigns in salesforce.com. Complete tracking of all required campaign fields for all online marketing.
        * SMB Customer Advocacy: Achieve 200% of SMB IACV plan.
        * SMB Customer Advocacy: Improve SMB account management by increasing customer gross retention to 100% and net retention to 120%. 
        * SMB Customer Advocacy: Help customers maximize the product’s value by communicating product issues to relevant teams. Develop relationships and provide strategic guidance/advanced troubleshooting. Restructure our onboarding process to reflect key initiatives. 
### CEO: Popular next generation product. Complete DevOps GA, GitLab.com ready for mission critical applications, graph of DevOps score vs. cycle time.

* VPE
  * Frontend: Implement 10 [performance improvements](https://gitlab.com/groups/gitlab-org/frontend/-/epics/2): X/10 (X%)
    * Frontend Discussion: Implement 5 [performance improvements](https://gitlab.com/groups/gitlab-org/frontend/-/epics/3) on the MR/issue page X/10 (X%)
    * Frontend MD&P: Implement and integrate 10 [reusable Vue components](https://gitlab.com/groups/gitlab-org/frontend/-/epics/1) into GitLab X/10 (X%)
  * Dev Backend
    * Discussion: Implement 10 [performance improvements](https://gitlab.com/groups/gitlab-org/-/epics/247): X/10 (X%)
    * Distribution: GitLab Helm charts generally available
      * Release: Automatically deploy to staging.gitlab.com when creating release
    * Geo: Implement 5 performance improvements (P1/P2/P3 categories): X/5 (X%)
    * Gitaly: Ship v1.0 and turn off NFS
    * Platform: Implement 10 [performance improvements](https://gitlab.com/groups/gitlab-org/-/epics/246): X/10 (X%)
    * Gitter: Open source Android and iOS applications
    * Gitter: Deprecate Gitter Topics
  * Ops Backend
    * CI/CD: Implement one key architectural performance improvement from https://gitlab.com/gitlab-org/gitlab-ce/issues/46499,
    * Configuration: API first Feature Flags: ship a feature flags MVC, all frontend code interacts via API to server, no server side rendering for feature flags
    * Monitoring: Implement admin performance dashboad. (Design issue TBD)
    * Security Products: Aggregate and store vulnerabilities occurrences into database
      ([&241](https://gitlab.com/groups/gitlab-org/-/epics/241))
* VPE: Make GitLab.com ready for mission-critical customer workloads (99.95% availability)
  * Frontend: 100% uptime according to TBD budget
    * Frontend Discussion: 100% uptime according to TBD budget
    * Frontend MD&P: 100% uptime according to TBD budget
  * Dev Backend
    * Discussion: 100% uptime according to TBD budget
    * Distribution: 100% uptime according to TBD budget
    * Geo
       * Finish failover to Google Cloud
       * 100% uptime according to TBD budget
    * Gitaly: 100% uptime according to TBD budget
    * Gitter: 100% uptime according to TBD budget
    * Platform: 100% uptime according to TBD budget
  * Infrastructure
    * Database: 100% uptime according to TBD budget
    * Production: 100% uptime according to TBD budget
  * Ops Backend
    * CI/CD: 100% uptime according to https://gitlab.com/gitlab-com/infrastructure/issues/4450,
    * Configuration: 100% uptime according to TBD budget
    * Monitoring: 100% uptime according to TBD budget
    * Security Products: 100% uptime according to TBD budget
  * Quality
    * Implement Review Apps for CE/EE with gitlab-qa test automation validation as part of every Merge Request CI pipeline.
    * Triage and add proper priority and severity labels to 80% CE/EE bugs with no priority and severity labels.    
  * UX
    * Establish the User Experience direction for the security dashboard: Aid in completing a Competitive analysis, identify 10 must have items for the security dashboard, complete a design artifact/discovery issue.
    * UX Research
      * Incorporate personas into our design process for 100% of our product features.
      * Identify 5 pain points for users who have left GitLab.com. Work with Product Managers to identify solutions.
  * Security
    * Identify and push top 10 sources/attributes to S3 instance and ensure at least 90 day retention and tooling for security investigations.
    * Triage 50 backlogged security labeled issues and add appropriate severity and priority labels.  
* CMO: Driving brand awareness/ preference through event presence.
    * Get 3 customer/ user talks accepted to enterpise events.
    * Build 1 cohesive brand strategy across all live events.
    * Increase web and twitter traffic by 50% as direct result of event activity.

### CEO: Great team. ELO score per interviewer, Employer branding person, Real-time dashboard for all Key Results.

* VPE: 10 iterations to engineering function handbook: X/10 (X%)
  * Infrastructure: 10 iterations to infrastructure department handbook: X/10 (X%)
    * Production: 10 iterations to production team handbook: X/10 (X%)
  * Ops Backend: 10 iterations to ops backend department handbook: X/10 (X%)
  * Ops Backend: Define and launch a dashboard with 2 engineering metrics for backend teams
  * Quality: 10 iterations to quality department handbook: X/10 (X%)
  * Security: 10 iterations to security department handbook: X/10 (X%)
  * Support: 20 iterations to support department handbook: X/20 (X%)
      * Self-hosted: 10 iterations to self-hosted team handbook focused on process efficiency: X/10 (X%)
      * Services: 10 iterations to services team handbook focused on process efficiency: X/10 (X%)
  * Support: Implement dashboard for key support metrics (SLA, CSAT)

* VPE: Source 300 candidates for various roles: X sourced (X%)
  * Frontend: Source 100 candidates by July 15 and hire 4 in total: X sourced (X%), hired X (X%)
    * Frontend Discussion: Hire 1: hired X (X%)
    * Frontend MD&P: Hire 1: hired X (X%)
  * Dev Backend: Create and apply informative template for team handbook pages
    across department
  * Dev Backend: Create well-defined hiring process for ICs and managers
    documented in handbook, ATS, and GitLab projects
  * Dev Backend: Source 20 candidates by July 15 and hire 2: 0 sourced (0%), hired 0 (0%)
    * Discussion: Source 50 candidates by July 15 and hire 2: X sourced (X%), hired X (X%)
    * Distribution: Source 15 candidates by July 15 and hire 1: X sourced (X%), hired X (X%)
    * Gitaly: Source 30 candidates by July 15 and hire 3: 0 sourced (0%), hired 0 (0%)
    * Platform: Source 75 candidates by July 15 and hire 3: X sourced (X%), hired X (X%)
  * Infrastructure: Source X candidates by July 15 and hire X: X sourced (X%), hired X (X%)
    * Database: Source X candidates by July 15 and hire X: X sourced (X%), hired X (X%)
    * Production: Source X candidates by July 15 and hire X: X sourced (X%), hired X (X%)
  * Ops Backend: Source 10 candidates by July 15 and hire 1 manager: X sourced (X%), hired X (X%)
    * CI/CD: Source 60 candidates by July 15 and hire 2: X sourced (X%), hired X (X%)
    * Configuration: Source 60 candidates by July 15 and hire 2: 0 sourced (0%), hired 0 (0%)
    * Monitoring: Source 100 candidates by July 15 and hire 2: X sourced (X%), hired X (X%)
    * Security Products: Source 50 candidates by July 15 and hire 2: 0 sourced (0%), hired 0 (0%)
  * Quality: Source 100 candidates by July 15 and hire 2: X sourced (X%), hired X (X%)
  * Security: Source 100 candidates by July 15 and hire 3: X sourced (X%), hired X (X%)
  * Support: Source 100 candidates by July 15 and hire EMEA and APAC managers: X sourced (X%), hired X (X%)
    * Self-hosted: Source 350 candidates by July 30 and hire 7: X sourced (X%), hired X (X%)
    * Services: Source 200 candidates by July 30 and hire 4: X sourced (X%), hired X (X%)
  * UX: Source X candidates by July 15 and hire X: X sourced (X%), hired X (X%)
* CFO: Improve payroll and payments to team members
  * Controller: Analysis and proposal on trinet conversion including cost/benefit analysis of making the move (support from peopleops)
  * Controller: Transition expense reporting approval process to payroll and payments lead.
  * Controller: Full documentation of payroll process (sox compliant)
* CFO: Improve financial performance
  * Fin Ops: Marketing pipeline driven revenue model (need assistance from marketing team)
  * Fin Ops: Recruiting / Hiring Model: redesign gitlab model so that at least 80% of our hiring can be driven by revenue.
  * Fin Ops: Customer support SLA metric on dashboard
